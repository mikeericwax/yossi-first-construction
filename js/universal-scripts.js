// HOME PAGE ADD CLASS WHEN READY
$(document).ready(function () {
    $('.slide1').addClass('animate');
});



// SERVICES POPUPS
$(document).ready(function () {
    var container = $('.service-popups'),
        popup = $('.service-popups .popup'),
        close = $('.service-popups .popup .close');
    
    //console.log(popupID);
    
    /*if (popup.data() == $('.slide .services ul li').data()) {
        popupID.addClass('active');
    }*/
    
    $('#BathroomService').click(function () {
        $('#BathroomPopup').addClass('active');    
    });
    
    $('#KitchenService').click(function () {
        $('#KitchenPopup').addClass('active');    
    });
    
    $('#ElectricalService').click(function () {
        $('#ElectricalPopup').addClass('active');    
    });
    
    $('#PlumbingService').click(function () {
        $('#PlumbingPopup').addClass('active');    
    });
    
    $('#CarpentryService').click(function () {
        $('#CarpentryPopup').addClass('active');    
    });
    
    $('#ConcreteService').click(function () {
        $('#ConcretePopup').addClass('active');    
    });
    
    $('#WindowsService').click(function () {
        $('#WindowsPopup').addClass('active');    
    });
    
    $('#RoofingService').click(function () {
        $('#RoofingPopup').addClass('active');    
    });
    
    close.click(function () {
        popup.removeClass('active');  
    });
});


// HIDE EVERYTHING IF GALLERY IS IN FULL SCREEN
$(document).ready(function () {
    var topNav = $('.slider-pagination'),
        footer = $('.footer'),
        fullScreen = $('span.change-mode');
    
    
    
    fullScreen.click(function () {
        console.log(fullScreen);
    });
});
